# MyTestCase REST API

## Подготовка 
**Entity**<br/> 
Создан класс Employee который отражает структуру генерирующейся таблицы employees. 
```java
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)  //Auto increment when new value inserted
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String position;

    @Column(nullable = false)
    private Double salary;
    
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date createdAt;

    @Column(nullable = false)
    private Boolean isWorking; 
    
    //getters/setters
    ...
```
**Repository** <br/>
Создан класс **EmployeeRepo** который является репозиторием и позволяет совершать операции над данными из БД.
```java
@Repository
public interface EmployeeRepo extends   JpaRepository<Employee, Long>, 
                                        JpaSpecificationExecutor,
                                        PagingAndSortingRepository<Employee, Long> {
	
	@Query(value = "select * from employees where created_at >= ?1", nativeQuery = true)
	List<Employee> gtCreatedAt(Optional<Date> date);
	@Query(value = "select * from employees where created_at <= ?1", nativeQuery = true)
	List<Employee> ltCreatedAt(Optional<Date> date);
	@Query(value = "select * from employees where created_at >= ?1 and created_at <= ?2", nativeQuery = true)
	List<Employee> rangeCreatedAt(Optional<Date> date1, Optional<Date> date2);
	
	public List<Employee> findAllByOrderBySalaryDesc();
	public Page<Employee> findAllByOrderBySalaryDesc(Pageable pageble);
	
	public List<Employee> findAllByOrderBySalaryAsc();
	public Page<Employee> findAllByOrderBySalaryAsc(Pageable pageble);
	
	public List<Employee> findAllByIsWorking(Boolean isWorking);
	public Page<Employee> findAllByIsWorking(Boolean isWorking, Pageable pageble);
	
}
```

## **Варианты запросов апросы**:
1.  **GET**
    *   **url/company/employees** <br/> 
        возвращает всех сотрудников в БД.
    *  **url/company/employees/{id}** <br/>
        Параметры: <br/>
        - id - id работника в БД. <br/>
        Возвращает сотрудника с заданным id.
    *  **url/company/employees/pages?page=&size=**<br/>
        Параметры: <br/>
        - page - номер страницы. <br/>
        - size - размер страницы. <br/>
        Возвращает список сотрудников с учетом пагинации.
    *  **url/company/employees/working?isWorking=&page=&size=**<br/>
        Параметры: <br/>
        - isWorking - статус сотрудника, true - действующий сотрудник, false - ушедший сотрудник <br/>
        - page - номер страницы. <br/>
        - size - размер страницы. <br/>
        Возвращает список сотрудников в БД с учетом пагинации и статусом работает или нет. 
        При этом если isWorking не задан то по умолчанию считается true. 
    *  **url/company/employees/sort?direction=&page=&size=** <br/>
        Параметры: <br/>
        - direction - сортирвка по возрастанию(asc), сортировка по убыванию(desc) <br/>
        - page - номер страницы. <br/>
        - size - размер страницы. <br/>
        Возвращает список работников в БД с учетом пагинации и отсортированных по полю salary.
    *  **url/company/employees/date?start=&end=** <br/>
        Параметры: <br/>
        - start - начальная дата. <br/>
        - end - конечная дата. <br/>
        Возвращает сотрудников в БД с учетом даты. Если есть только значение start то ищет записи у которых дата >= start.<br/>
        Формат даты: dd.MM.yyyy.
    * **url/company/employees/filter?name=&position=&salary=&createAt=&isWorking=** <br/>
        Параметры: <br/>
        - isWorking - статус работника, true - действующий сотрудник, false - ушедший сотрудник <br/>
        - salary - зарплата сотрудника. <br/>
        - name - имя сотрудника.
        - position - должность сотрудника.
        - createAt - дата создания/модификации.<br/>
        Возвращает отфильтрованный список сотрудников в БД. Для полей name и position генерируется like запрос например: select * ... where name like 'value'. <br/>
        Для остальных генирируется запрос с equal например: select * ... where salary = value. Параметры можно комбинировать.
2.  **POST**
    * **url/company/employees** <br/>
        Параметры:
        - тело запроса.<br/>
        Пример тела запроса:
        ```json
        {
            "name":"Name1",
            "position":"Pos1",
            "salary":1000.0
        }
        ```
        Задавать поля isWorking и createdAt не нужно так как они генерируются автоматический при создании.<br/>
        Возвращает только что созданного сотрудника.
3.  **PUT**
    * **url/company/employees/{id}**<br/>
        Параметры:
        - id - id сотрудника которого надо изменить
        - тело запроса(задается так же как и при POST запросе)<br/>
        Возвращает измененного сотрудника.
4.  **DELETE**
    * **url/company/employees/{id}**<br/>
        Параметры:
        - id - id работника которого надо удалить.
        Проверяет имеется ли сотрудник в БД, если есть то удаляет его, если не производит никаких действий.Ничего не возвращает.
