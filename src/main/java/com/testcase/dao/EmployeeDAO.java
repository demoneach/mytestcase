package com.testcase.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.testcase.model.Employee;
import com.testcase.repository.EmployeeRepo;
import com.testcase.specifications.EmployeeSpec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


/*
 *DAO - data access object 
 *Here we will insert/select etc. data in/from db */
@Service
public class EmployeeDAO {
	@Autowired
	EmployeeRepo employeeRepository;
	
	public Employee save(Employee empl) {
		try {
			return employeeRepository.save(empl);
		}
		catch(Exception ignored){
			return null;
		}
	}


	public List<Employee> findAll() {
		return (List<Employee>) employeeRepository.findAll();
	}

	public List<Employee> findInRange(Long page, Long size) {
		try {
			Pageable pageble = PageRequest.of(Math.toIntExact(page), Math.toIntExact(size));
			return employeeRepository.findAll(pageble).toList();
		}
		catch(Exception ignored){
			return null;
		}
		
	}
	
	public List<Employee> sortBySalaryDesc(Integer page, Integer size) {
		Pageable pageble = PageRequest.of(Math.toIntExact(page), Math.toIntExact(size));
		return employeeRepository.findAllByOrderBySalaryDesc(pageble).toList();
	}
	public List<Employee> sortBySalaryAsc(Integer page, Integer size) {
		Pageable pageble = PageRequest.of(Math.toIntExact(page), Math.toIntExact(size));
		return employeeRepository.findAllByOrderBySalaryAsc(pageble).toList();
	}
	public List<Employee> findWorkingEmployees(Boolean isWorking, Integer page, Integer size) {
		try {
			Pageable pageble = PageRequest.of(Math.toIntExact(page), Math.toIntExact(size));
			return employeeRepository.findAllByIsWorking(isWorking,pageble).toList();
		}
		catch(Exception ignored){
			return null;
		}
	}

	public List<Employee> findFilteredEmployees(Employee empl) {
		try {
			EmployeeSpec spec = new EmployeeSpec(empl);
			return employeeRepository.findAll(spec);
		}
		catch(Exception ignored){
			return null;
		}
	}
	
	public List<Employee> findGTDate(Optional<Date> date) {
		try {
			return employeeRepository.gtCreatedAt(date);
		}
		catch(Exception ignored){
			return null;
		}
	}
	public List<Employee> findLTDate(Optional<Date> date) {
		try {
			return employeeRepository.ltCreatedAt(date);
		}
		catch(Exception ignored){
			return null;
		}
	}
	public List<Employee> findBetween(Optional<Date> date1, Optional<Date> date2) {
		try {
			return employeeRepository.rangeCreatedAt(date1, date2);
		}
		catch(Exception ignored){
			return null;
		}
	}
	public Employee getOne(Long emplId) {
		try {
			return employeeRepository.findById(emplId).orElse(null);
		}
		catch(Exception ignored){
			return null;
		}
	}

	public void deleteById(Long emplId) {
		try {
			employeeRepository.deleteById(emplId);
		}
		catch(Exception ignored){
			
		}
	}

	public void delete(Employee empl) {
		try {
			employeeRepository.delete(empl);
		}
		catch(Exception ignored){
			
		}
	}
}
