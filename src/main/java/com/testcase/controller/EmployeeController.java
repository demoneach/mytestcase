package com.testcase.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.testcase.dao.EmployeeDAO;
import com.testcase.model.Employee;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/company") // root
public class EmployeeController {

	@Autowired
	EmployeeDAO employeeDAO;

	/**
	 * @param empl сериализованный работник из запроса.
	 * @return возвращает созданного работника.
	 */
	@PostMapping("/employees")
	public Employee createEmployee(@Valid @RequestBody Employee empl) {
		empl.setIsWorking(true);
		return employeeDAO.save(empl);

	}


	/**
	 * @return всех работников в базе.
	 */
	@GetMapping("/employees")
	public List<Employee> getAllEmployees() {
		try {
			return employeeDAO.findAll();
		} catch (Error e) {
			return null;
		}
	}

	/**
	 * 
	 * @param emplId id работника
	 * @return возвращает работника если таковой имеется.
	 */

	@GetMapping("/employees/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable(value = "id") Long id) {
		Employee empl = employeeDAO.getOne(id);
		if (empl == null)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(empl);
	}


	@GetMapping("/employees/pages")
	public ResponseEntity<List<Employee>> getEmployeesById(@RequestParam Long page, @RequestParam Long size) {
		List<Employee> empls = employeeDAO.findInRange(page, size);
		if (empls == null || empls.size() == 0)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(empls);

	}

	/**
	 * @param isWorking если не задан по умолчанию true.
	 * @param page номер страницы.
	 * @param size кол-во.
	 * @return выводит список сотрудников которые работают(true), не работают(false).
	 */
	@GetMapping("/employees/working")
	public ResponseEntity<List<Employee>> getWorkingEmployees(  @RequestParam Optional<Boolean> isWorking,
																@RequestParam Integer page,
																@RequestParam Integer size) {
		List<Employee> empls = null;
		if(!isWorking.isEmpty())
			empls = employeeDAO.findWorkingEmployees(isWorking.get(), page, size);
		else empls = employeeDAO.findWorkingEmployees(true, page, size);
		if (empls == null || empls.size() == 0)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(empls);
	}
	/**
	 * @param direction сортировать по возрастанию(asc), по убыванию(desc).
	 * @param page номер страницы.
	 * @param size кол-во.
	 * @return выводит сотрудников сортированных по зарплате.
	 */
	@GetMapping("/employees/sort") 
	public ResponseEntity<List<Employee>> getEmployeeSortedBySalary(@RequestParam String direction,
																	@RequestParam Integer page,
																	@RequestParam Integer size) {
		List<Employee> empls = null;
		if(direction.toLowerCase().equals("asc")) {
			empls = employeeDAO.sortBySalaryAsc(page,size);
		}
		else if(direction.toLowerCase().equals("desc")) {
			empls = employeeDAO.sortBySalaryDesc(page,size);
		}
		if (empls == null || empls.size() == 0)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(empls);
	}

	/**
	 * Формат даты dd.MM.yyyy.
	 * Если задана только начальная дата, то идет поиск значений которые больше этой даты.
	 * Если задана только конечная дата, то идет поиск которые меньше этой даты.
	 * Если заданы оба значения, то поиск осуществляется между ними. 
	 * @param start начальная дата
	 * @param end конечная дата
	 * @return ответ на запрос.
	 */
	@GetMapping("/employees/date")
	public ResponseEntity<List<Employee>> getEmployeesByDate(@RequestParam 
															  @DateTimeFormat(pattern="dd.MM.yyyy") Optional<Date> start,
															  @RequestParam 
															  @DateTimeFormat(pattern="dd.MM.yyyy") Optional<Date> end) {
		List<Employee> empls = null;
		if(start != null) empls = employeeDAO.findGTDate(start);
		else if(end != null) empls = employeeDAO.findLTDate(end);
		else empls = employeeDAO.findBetween(start, end);
		if(empls == null || empls.size() == 0)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(empls);
	}
	/**
	 * Все параметры опциональны. Находит всех работников удовлетворяющих параметрам.
	 * @param name string использует like.
	 * @param position string использует like
	 * @param salary double использует equal
	 * @param createdAt date шаблон "dd.MM.yyyy" использует equal
	 * @param isWorking Boolean использует equal
	 * @return Список найденных служащих котоыре удовлетворяют параметрам
	 */
	@GetMapping("/employees/filter")
	public ResponseEntity<List<Employee>> getEmployeesByFilter( @RequestParam Optional<String> name,
																@RequestParam Optional<String> position,
																@RequestParam Optional<Double> salary,
																@RequestParam 
																@DateTimeFormat(pattern="dd.MM.yyyy") Optional<Date> createdAt,
																@RequestParam Optional<Boolean> isWorking) {
		List<Employee> empls = null;
		Employee empl = new Employee();
		if(!name.isEmpty())
			empl.setName(name.get());
		if(!position.isEmpty())
			empl.setPosition(position.get());
		if(!salary.isEmpty())
			empl.setSalary(salary.get());
		if(!createdAt.isEmpty())
			empl.setCreatedAt(createdAt.get());
		if(!isWorking.isEmpty())
			empl.setIsWorking(isWorking.get());
		empls = employeeDAO.findFilteredEmployees(empl);
		if(empls == null || empls.size() == 0)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(empls);
	}

	/**
	 * @param emplId id работника которого надо обновить.
	 * @param newEmpl новый работник.
	 * @return возвращает обновленного работника.
	 */
	@PutMapping("/employees/{id}")
	public ResponseEntity<Employee> updateEmployeeById(@PathVariable(value = "id") Long emplId,
			@Valid @RequestBody Employee newEmpl) {
		Employee empl = employeeDAO.getOne(emplId);
		if (empl == null)
			return ResponseEntity.notFound().build();
		empl.setName(newEmpl.getName());
		empl.setPosition(newEmpl.getPosition());
		empl.setSalary(newEmpl.getSalary());
		if(newEmpl.getIsWorking() != null)
			empl.setIsWorking(newEmpl.getIsWorking());
		else empl.setIsWorking(true);
		Employee updatedEmployee = employeeDAO.save(empl);
		return ResponseEntity.ok().body(updatedEmployee);
	}

	/**
	 * @param emplId id работникаа которого надо удалить из базы.
	 * @return возвращает ok если работник был успешно удален.
	 */
	@DeleteMapping("/employees/{id}")
	public ResponseEntity<Employee> deleteEmployeeById(@PathVariable(value = "id") Long emplId) {
		Employee empl = employeeDAO.getOne(emplId);
		if (empl == null)
			return ResponseEntity.notFound().build();
		employeeDAO.deleteById(emplId);
		return ResponseEntity.ok().build(); // ResponseEntity.ok().body(empl);
	}
}
